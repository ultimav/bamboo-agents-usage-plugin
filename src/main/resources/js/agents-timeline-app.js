define('plugin/agents-usage/agents-usage-app', [
    'underscore',
    'jquery',
    'backbone',
    'plugin/agents-usage/d3',
    'plugin/agents-usage/d3-timeline',
    'util/ajax'
], function(_,
            $,
            Backbone,
            d3,
            d3Timeline,
            ajax) {
    return Backbone.View.extend({
        options: {
            $el: null,
            $inputFilter: null,
            $startDatePicker: null,
            $endDatePicker: null,
            $updateButton: null
        },
        defaults: {
            url: AJS.contextPath() + '/rest/agentsusage/latest/agentsusage',
            inputFilter: null,
            startDatePicker: null,
            endDatePicker: null,
            updateButton: null,
            tickFormat: '%b %d %H:%M',
            width: 1150,
            height: 1150
        },
        tooltip: null,
        initialize: function(options) {
            this.options = _.defaults(options, this.defaults);
            this.options.$el = $(this.options.el);
            this.options.$inputFilter = $(this.options.inputFilter);
            this.options.$startDatePicker = $(this.options.startDatePicker);
            var $startDatePicker = this.options.$startDatePicker;
            $startDatePicker.datePicker();
            var today = new Date();
            $startDatePicker.val(today.getFullYear() + '-' + this.addZeros(today.getMonth() + 1) + '-' + this.addZeros(today.getDate()) + 'T00:00');
            this.options.$endDatePicker = $(this.options.endDatePicker);
            var $endDatePicker = this.options.$endDatePicker;
            $endDatePicker.datePicker();
            $endDatePicker.val(today.getFullYear() + '-' + this.addZeros(today.getMonth() + 1) + '-' + this.addZeros(today.getDate()) + "T23:59");
            this.options.$updateButton = $(this.options.updateButton);
            this.options.$updateButton.on('click', _.bind(this.fetchData, this));
            this.fetchData();
            this.tooltip = d3.select('body').append('div')
                .attr('class', 'tooltip')
                .style('opacity', 0);
        },
        addZeros: function(num) {
            if (num < 10) {
                return '0' + num;
            } else {
                return '' + num;
            }
        },
        renderSplash: function() {
            this.options.$el.empty().html('<span>' + AJS.I18n.getText('agents.timeline.loading') + '</span>');
        },
        fetchData: function() {
            this.renderSplash();
            var filter = this.options.$inputFilter.val();
            var startDate = this.formatDate(this.options.$startDatePicker.val());
            var endDate = this.formatDate(this.options.$endDatePicker.val());
            ajax({
                type: 'GET',
                url: this.options.url,
                dataType: 'json',
                data: {
                    inputFilter: filter,
                    from: startDate,
                    to: endDate
                },
                success: _.bind(this.renderResponse, this),
                error: _.bind(this.handleError, this)
            });
        },
        /**
         *
         * @param dateString
         * @return string in format yyyy_MM_dd_HH_mm
         */
        formatDate: function(dateString) {
            var year = dateString.substr(0, 4);
            var month = dateString.substr(5, 2);
            var day = dateString.substr(8, 2);
            var hours = dateString.substr(11, 2);
            var minutes = dateString.substr(14, 2);
            return year + '_' + month + '_' + day + '_' + hours + '_' + minutes;
        },
        prepareData: function(data) {
            var min = Number.MAX_VALUE;
            var max = Number.MIN_VALUE;
            var result = $.map(data, function(item) {
                item.label = item.label || item.agentName;
                item.times = $.map(item.times, function(time) {
                    time.starting_time = time.starting_time || time.startTime;
                    time.ending_time = time.ending_time || time.endTime;
                    time.id = time.id || time.key;
                    if (time.starting_time < min) {
                        min = time.starting_time;
                    }
                    if (time.ending_time > max) {
                        max = time.ending_time;
                    }
                    return time;
                });
                item['class'] = 'rect';
                return item;
            });
            return {min: min, max: max, data: result};
        },
        renderResponse: function(jsonData) {
            this.options.$el.empty();
            var content = this.prepareData(jsonData);

            var leftMargin = 150;
            var chart = d3Timeline.stack() // toggles graph stacking
                .rotateTicks(30)
                .showAxisCalendarYear()
                .tickFormat({
                    format: d3.time.format(this.options.tickFormat),
                    tickInterval: 60,
                    numTicks: 30,
                    tickSize: 1
                })
                .showTimeAxisTick()
                .beginning(content.min)
                .ending(content.max)
                .margin({
                    left: leftMargin,
                    right: 30,
                    top: 0,
                    bottom: 0
                })
                .mouseover(_.bind(this.mouseOverHandler, this))
                .mouseout(_.bind(this.mouseOutHandler, this))
                .click(function(d, i, datum) {
                    if (d.key !== undefined) {
                        window.location = AJS.contextPath() + '/browse/' + d.key;
                    }
                });

            var svg = d3.select(this.options.el).append('svg').attr('width', this.options.width).attr('height', this.options.height)
                .datum(content.data).call(chart);
        },
        mouseOverHandler: function(d, i, datum) {
            var rect = d3.select('#' + d.id);
            if (rect == null) {
                return;
            }
            // d is the current rendering object
            // i is the index during d3 rendering
            // datum is the data object
            this.tooltip.transition()
                .duration(200)
                .style('opacity', .9);
            this.tooltip.html(d.key)
                .style('left', 170 + Number(rect.attr('x')) + 'px')
                .style('top', 150 + Number(rect.attr('y')) + 'px');
        },
        mouseOutHandler: function(d, i, datum) {
            this.tooltip.transition()
                .duration(500)
                .style('opacity', 0);
        },
        handleError: function(xhr) {
            this.options.$el.empty().html('<span>' + AJS.I18n.getText('agents.timeline.loading.failed') + '</span>');
        }
    });
});