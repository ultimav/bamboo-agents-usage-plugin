# README #

Plugin for Bamboo which shows builds in timeline view. 
Allows to see utilization of agents and server load. 
![agents_usage.png](https://bitbucket.org/repo/Bnj54A/images/3231498354-agents_usage.png)
# Installation #
Plugin can be installed from [Marketplace](https://marketplace.atlassian.com/plugins/com.atlassianlab.bamboo.plugins.bamboo-agents-usage/server/overview) or built from sources.
To build plugin, checkout repository and then execute
```
mvn clean package
```
then use Bamboo > Administration > Manage Add-ons > Upload addon and upload jar file from target folder

# Licenses #
d3 (https://d3js.org) - BSD License
d3-timeline (https://github.com/jiahuang/d3-timeline) - MIT license

### Who do I talk to? ###

* Alexey Chystoprudov, achystoprudov@atlassian.com

## License ##

    Copyright (c) 2016, Atlassian Pty. Ltd.
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    The names of contributors may not
    be used to endorse or promote products derived from this software without
    specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.