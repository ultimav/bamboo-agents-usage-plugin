define('plugin/agents-usage/d3-timeline', [
    'plugin/agents-usage/d3'
], function(d3) {
    window.AGENTS_USAGE_D3_TIMELINE = d3.timeline();
    return window.AGENTS_USAGE_D3_TIMELINE;
});